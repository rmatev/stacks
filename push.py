"""Push tags in chunks as GitLab can't handle all at once."""

import argparse
import os
import sys
import git
try:
    from itertools import zip_longest
except ImportError:
    from itertools import izip_longest


def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


parser = argparse.ArgumentParser()
parser.add_argument('repo', help='Path to local git repo.')
args = parser.parse_args()

repo = git.Repo.init(args.repo)

# remote_refs = dict(reversed(x.split('\t')) for x in repo.git.ls_remote('origin').splitlines())
# push_tags = [tag.name for tag in repo.tags if 'refs/tags/' + tag.name not in remote_refs]

chunks = list(grouper(repo.tags, 25))
for i, tags in enumerate(chunks):
    print('Pushing chunk {}/{}'.format(i + 1, len(chunks)))
    repo.git.push('origin', *filter(None, tags))
