from __future__ import print_function, division
import argparse
import logging
import os
import re
from collections import defaultdict
from pprint import pformat

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)-8s %(message)s',
    datefmt='%H:%M:%S')
log = logging.getLogger()


class TagException(Exception):
    pass


def int_or_default(x, default):
    return default if x is None else int(x)


def parse_version(s):
    m = re.match(r'^v(?P<v>\d*)r(?P<r>\d*)(p(?P<p>\d*))?(g(?P<g>\d*))?$', s)
    return tuple(int_or_default(m.group(i), -1) for i in 'vrpg')


def memoize(func):
    cache = func.cache = {}

    def decorated(x):
        try:
            y = cache[x]
        except KeyError:
            y = cache[x] = func(x)
        return y

    return decorated


def get_soft_db():
    try:
        instance = get_soft_db._instance
    except AttributeError:
        from LbSoftConfDb2Clients.GenericClient import LbSoftConfDbBase
        generic_client = LbSoftConfDbBase()
        instance = get_soft_db._instance = generic_client.getROInterface(
            noCertVerif=True)
    return instance


@memoize
def get_project_properties(name):
    src = get_soft_db().getProjectProperties(name)['sourceuri']
    m = re.match(r'^gitlab-cern:?(?P<ns>.+)/(?P<name>[^/]+)$', src)
    return {
        'ns': m.group('ns'),
        'name': m.group('name'),
        'url':
        'https://gitlab.cern.ch/{ns}/{name}.git'.format(**m.groupdict()),
    }


def get_pv_deps(project, version):
    """Get the immediate upstream deps of a project/version."""
    project = project.upper()
    if project == 'GAUDI':
        # ignore LCG dependency
        return {}
    all_deps = get_soft_db().listDependencyRelations(project, version)
    deps = defaultdict(list)
    for ((p, v), (dep_p, dep_v)) in all_deps:
        if p == project and v == version:
            deps[dep_p].append(dep_v)

    for p, vs in deps.items():
        if len(vs) > 1:
            vs.sort(key=parse_version)
            log.warning('Duplicate depency in SDB: {}/{} -> {}/{}. '
                        'Using last.'.format(project, version, p, vs))
    return {p: vs[-1] for p, vs in deps.items()}


def list_packages(commit):
    """List all gaudi packages in a commit's tree."""
    packages = []
    for blob in commit.tree.traverse():
        try:
            if (blob.name == 'CMakeLists.txt'
                    and (not blob.path.startswith('CMakeLists.txt'))
                    and (not blob.path.startswith('cmake'))):
                packages.append(os.path.dirname(blob.path))
        except AttributeError as e:
            # TODO submodules are ignored because I don't understand this:
            if ('Cannot retrieve the name of a submodule if it was not '
                    'set initially' not in str(e)):
                raise
    return packages


def write_project(repo, project, version):
    log.debug('Checking out packages from {}/{}'.format(project, version))
    path = repo.working_tree_dir
    if project == 'LCG':
        name = 'LCG'
        fn = os.path.join(path, name)
        with open(fn, 'w') as f:
            f.write(version + '\n')
        repo.index.add([name])
        return ''

    props = get_project_properties(project)
    name = props['name']

    try:
        remote = repo.remote(name)
    except ValueError:
        remote = repo.create_remote(name, props['url'])
        remote.fetch(
            refspec="+refs/tags/*:refs/remotes/project/{}/*".format(name),
            no_tags=True)
    # repo.git.read_tree(name + '/' + version, prefix=name, u=True)

    tag = 'project/{}/{}'.format(name, version)
    try:
        commit = repo.rev_parse(tag)
    except git.BadName:
        raise TagException('Unknown tag {} for {}'.format(version, name))
    if commit.type == 'tag':
        commit = commit.object
    packages = list_packages(commit)

    overwritten = sorted(
        set(
            os.path.dirname(b.path) for s, b in repo.index.iter_blobs()
            if os.path.dirname(b.path) in packages))
    if overwritten:
        log.debug('Packages to overwrite:', pformat(overwritten))
        # If packages to be addeded exist already, remove them
        repo.index.remove(
            packages,
            r=True,
            working_tree=True,
            ignore_unmatch=True,
            force=True)

    repo.git.checkout(tag, '--', '*', recurse_submodules=True)

    for n in os.listdir(path):
        if n == 'cmt' or (os.path.isfile(os.path.join(path, n))
                          and n != 'LCG'):
            try:
                root_dir = os.path.join(path, 'root', project)
                os.makedirs(root_dir)
            except OSError:
                pass
            repo.index.move(items=[n, os.path.join(root_dir, n)])

    return commit.hexsha


def tag_stack(repo, project, version):
    project = get_project_properties(project)['name']
    tag_name = project + '/' + version
    try:
        return repo.tags[tag_name]
    except IndexError:
        pass

    deps = sorted(get_pv_deps(project, version).items())
    base_tags = []
    for p, v in deps:
        base_tags.append(tag_stack(repo, p, v))
    log.debug('Tagging stack {}/{}'.format(project, version))

    message = ['{} {} stack'.format(project, version), '']
    if base_tags:
        # The first dependency is the base. In case of multiple dependencies,
        # all but the first are committed together with the project itself.
        base_tag = base_tags[0]
        repo.git.checkout(base_tag)
        log.debug('Base tag is {}'.format(base_tag.name))
        message += base_tag.object.message.splitlines()[2:]
    else:
        try:
            repo.git.branch('tmp', delete=True, force=True)
        except git.GitCommandError:
            pass
        repo.git.checkout(orphan='tmp')
        repo.index.remove(
            '.', r=True, working_tree=True, force=True, ignore_unmatch=True)

    # The first dependency is our base, so remove it
    for p, v in deps[1:] + [(project, version)]:
        sha = write_project(repo, p, v)
        message.append('{}\t{}\t({})'.format(p, v, sha))

    c = repo.index.commit('\n'.join(message))
    return repo.create_tag(tag_name, c)


parser = argparse.ArgumentParser()
parser.add_argument('repo', help='Path to local git repo.')
parser.add_argument('project', help='Project to tag.')
parser.add_argument(
    '--versions',
    nargs='*',
    help='Specific versions to tag (all if not given).')
parser.add_argument('--debug', action='store_true', help='Debug information.')
args = parser.parse_args()

sdb = get_soft_db()

if args.debug:
    os.environ['GIT_PYTHON_TRACE'] = 'full'
import git  # import git after setting GIT_PYTHON_TRACE

if not args.versions:
    args.versions = sorted([v for _, v in sdb.listVersions(args.project)],
                           key=parse_version)

try:
    repo = git.Repo.init(args.repo)
except git.InvalidGitRepositoryError:
    log.error("'{}' is not a Git repository".format(args.repo))
    exit(1)

if repo.is_dirty(untracked_files=True):
    log.error("'{}' is dirty, aborting".format(args.repo))
    exit(1)

project = args.project
for i, version in enumerate(args.versions):
    progress = i / len(args.versions)
    log.info('[{:3.0f}%] Tagging stack {}/{}'.format(progress * 100, project,
                                                     version))
    try:
        tag_stack(repo, project.upper(), version)
    except TagException as e:
        log.warning('Failed: ' + str(e))
