# Tagged release stacks

This repository provides tags of entire released stacks.
This means that doing a diff between `Moore/v24r2` and `Moore/v24r3`
will also show differences in upstream projects, such as `Hlt` and not
only the differences in `Moore` itself. "Borrowed" packages are
handled correctly (i.e. the upstream package is entirely replaced).
As you might expect, differences are typically very long.

## How to use

### Directly from GitLab

Since diffs are long, the web interface is not very appropriate.
In any case, tags can be compared in the dedicated [page](/../../compare/),
like for instance [Moore/v24r2...Moore/v24r3](/../../compare/Moore/v24r2...Moore/v24r3).

### With a local clone

```sh
git clone https://gitlab.cern.ch/rmatev/stacks.git
cd stacks
```

#### Project versions (dependencies) in a stack

```sh
git log -1 Moore/v24r2
```

```console
commit 5f86742866f06683e7d73a5e4134eac3b8129413 (tag: Moore/v24r2)
Author: Rosen Matev <rosen.matev@cern.ch>
Date:   Thu Jul 9 13:47:28 2020 +0200

    Moore v24r2 stack

    Gaudi   v26r3   (e7c177922abae5e4c76ebeee46dcc640cb62da12)
    LHCb    v39r0   (f2f80dbcab805129e9aa1ebb1a35d134d06dbc30)
    Lbcom   v17r0   (11d2d4121c2789b08207c18bc52ce619c2f955d9)
    Rec     v18r1   (b57172fbf051d2484e4aaf56d97e3bc33132a188)
    Phys    v20r1   (282d5165e48451562b3544b3e0723a02773db8d9)
    Hlt     v24r2   (ac67aecb9f838865989c8390a10568dd4bee9a64)
    Moore   v24r2   (d12026e1f3322df64a12da7ee8231410b6e1fcf6)
```

#### Summary of changes between two stacks

```sh
git diff --stat Moore/v24r2..Moore/v24r3
```

```console
 Hlt/Hlt1Lines/CMakeLists.txt                       |    2 +-
 Hlt/Hlt1Lines/cmt/requirements                     |    2 +-
 Hlt/Hlt1Lines/doc/release.notes                    |   73 +
 Hlt/Hlt1Lines/python/Hlt1Lines/Hlt1BeamGasLines.py |   10 +-
 Hlt/Hlt1Lines/python/Hlt1Lines/Hlt1CEPLines.py     |  108 --
 .../python/Hlt1Lines/Hlt1CalibRICHMirrorLines.py   |   15 +-
 .../python/Hlt1Lines/Hlt1CalibTrackingLines.py     |   10 +-
 .../python/Hlt1Lines/Hlt1CommissioningLines.py     |   42 +-
 Hlt/Hlt1Lines/python/Hlt1Lines/Hlt1GECs.py         |   42 +-
 ...
 MooreSys/doc/release.notes                         |   14 +
 root/Moore/CMakeLists.txt                          |    2 +-
 100 files changed, 5496 insertions(+), 644 deletions(-)
```

#### Diff of a package between two stacks

```sh
git diff Moore/v24r2..Moore/v24r3 -- Hlt/HltGlobalMonitor
```

```diff
diff --git a/Hlt/HltGlobalMonitor/CMakeLists.txt b/Hlt/HltGlobalMonitor/CMakeLists.txt
index c2934e4c92..0587b49172 100644
--- a/Hlt/HltGlobalMonitor/CMakeLists.txt
+++ b/Hlt/HltGlobalMonitor/CMakeLists.txt
@@ -1,7 +1,7 @@
 ################################################################################
 # Package: HltGlobalMonitor
 ################################################################################
-gaudi_subdir(HltGlobalMonitor v5r2)
+gaudi_subdir(HltGlobalMonitor v5r3)

 gaudi_depends_on_subdirs(Event/HltEvent
                          Event/L0Event
...
```

### Advanced local use examples

#### Summary of complete file removals or additions

```sh
git diff --stat --diff-filter=m --find-renames=100% Brunel/v51r1..Brunel/v50r7
```

#### Filter out comments and uninteresting files

(needs git 2.30)

```sh
git diff -w -I'^ *(/\*|\* |\\\*|#|//)' Brunel/v51r1..Brunel/v50r7 -- \
    . ':(exclude)*/cmt/*' ':(exclude)*/COPYING' ':(exclude)*/doc/*'
```

Same as above, but only show modified files (not complete file removals or additions).

```sh
git diff -w -I'^ *(/\*|\* |\\\*|#|//)' --diff-filter=M --find-renames=100% Brunel/v51r1..Brunel/v50r7 -- \
    . ':(exclude)*/cmt/*' ':(exclude)*/COPYING' ':(exclude)*/doc/*' ':(exclude)*/CMakeLists.txt'
```

#### Show changes in reference files

```sh
git diff --diff-filter=M --no-renames  Brunel/v51r1..Brunel/v50r7 -- '*.ref'
```

#### Preprocess (format, add copyright) before diff

Checkout the stack tag that needs formatting, and format only the modified files with respect
to the other tag.

```sh
git checkout Brunel/v51r1
git diff --diff-filter=M --no-renames --name-only Brunel/v50r7 | xargs -r -n 1 -P 8 lb-format
git add -u
git commit -m 'format'
git diff --diff-filter=M --no-renames --name-only Brunel/v50r7 | xargs -r -n 1 -P 8 lb-add-copyright --year 2000-2020
git add -u
git commit -m 'copyright'
# git checkout -b Brunel/v51r1/pp

git diff --diff-filter=M --no-renames  Brunel/v50r7
```

## How to update tags

```sh
python tag_stacks.py . Moore
python tag_stacks.py . DaVinci
# ...
python push.py .
```
